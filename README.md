# ExentStore

## Running the app (dev)

  * Start the DB with `docker-compose up -d`
  * Install dependencies with `mix deps.get`
  * Create and migrate your database with `mix ecto.setup`
  * Install Node.js dependencies with `npm install` inside the `assets` directory
  * Start Phoenix endpoint with `iex -S mix phx.server`

Now you can visit [`localhost:4000`](http://localhost:4000) from your browser.

## Running the app (production)

The database must exist.

Start the app:

```bash
docker run --rm --name=exentstore --network="host" \
  -e SECRET_KEY_BASE=$(mix phx.gen.secret) \
  -e DATABASE_URL=ecto://postgres:postgres@localhost/exentstore \
  registry.gitlab.com/mwillema/exentstore:latest
```

Run the migrations:

```bash
docker exec -it exentstore bin/exentstore rpc "ExentStore.Release.migrate"
```

## REST API

### Append To Stream

Request

```bash
curl --request POST \
  --url http://localhost:4000/api/streams/MyStream \
  --header 'Content-Type: application/json' \
  --data '{
	"expected_version":6,
	"events": [
		{"event_type": "A", "event_body": "0"},
		{"event_type": "B", "event_body": "1"}
	]
}'
```

Success Reponse

`201 Created`

Error Response (Invalid Data)

`422 Unprocessable Entity`

```json
{
  "errors": {
    "events": [
      {
        "event_body": [
          "can't be blank"
        ]
      },
      {
        "event_type": [
          "can't be blank"
        ]
      }
    ],
    "expected_version": [
      "must be greater than or equal to 0"
    ],
    "stream_name": [
      "can't be blank"
    ]
  }
}
```

Error Response (Wrong Expected Version)

`422 Unprocessable Entity`

```json
{
  "actual_version": 6,
  "error": "wrong expected version"
}
```

### Event Stream Exists

Request

```bash
curl --request HEAD \
  --url http://localhost:4000/api/streams/MyStream
```

Success Response

`200 OK`

Error Response (Does Not Exist)

`404 Not Found`

### Load Event Stream

Request

```bash
curl --request GET \
  --url 'http://localhost:4000/api/streams/MyStream?after_stream_version=4'
````

Success Response

`200 OK`

```json
{
  "events": [
    {
      "event_body": "0",
      "event_id": 18,
      "event_type": "A",
      "occurred_on": "2021-02-15T16:05:00",
      "stream_name": "MyStream",
      "stream_version": 5
    },
    {
      "event_body": "1",
      "event_id": 19,
      "event_type": "B",
      "occurred_on": "2021-02-15T16:05:00",
      "stream_name": "MyStream",
      "stream_version": 6
    }
  ],
  "stream_version": 6
}
```

Error Response (Does Not Exist)

`404 Not Found`

Error Response (Invalid Data)

`422 Unprocessable Entity`

```json
{
  "errors": {
    "after_stream_version": [
      "must be greater than or equal to 0"
    ]
  }
}
```

### Delete Event Stream

Request

```bash
curl --request DELETE \
  --url http://localhost:4000/api/streams/MyStream
````

Success Response

`204 No Content`

Error Response (Does Not Exist)

`404 Not Found`

### Subscribe To All

Request

```bash
curl --request POST \
  --url http://localhost:4000/api/subscriptions \
  --header 'Content-Type: application/json' \
  --data '{
	"after_event_id": 0,
	"callback_url": "http://iam"
}'
````

Success Response

`201 Created`

```json
{
  "after_event_id": 0,
  "callback_url": "http://iam/",
  "subscription_id": 2
}
```

Error Response

`422 Unprocessable Entity`

```json
{
  "errors": {
    "after_event_id": [
      "must be greater than or equal to 0"
    ],
    "callback_url": [
      "must be unique"
    ]
  }
}
```

Callback URL

Every time some events are appended, the new events are delivered to the callback URL using a `POST` request and a JSON body such as the following:

```json
[
   {
      "event_id":7,
      "event_type":"A",
      "event_body":"0",
      "occurred_on":"2021-04-30T09:55:33",
      "stream_name":"MyStream",
      "stream_version":7
   },
   {
      "event_id":8,
      "event_type":"B",
      "event_body":"1",
      "occurred_on":"2021-04-30T09:55:33",
      "stream_name":"MyStream",
      "stream_version":8
   }
]
```

### Health Check

Request

```bash
curl --request GET \
  --url http://localhost:4000/api/health
````

Success Response

`200 OK`

```json
{
  "status": "UP"
}
```

Error Response

`503 Service Unavailable`

```json
{
  "reason": "Unreachable database",
  "status": "DOWN"
}
```
