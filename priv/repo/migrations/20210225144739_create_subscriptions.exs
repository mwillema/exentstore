defmodule ExentStore.Repo.Migrations.CreateSubscriptions do
  use Ecto.Migration

  def change do
    create table(:subscriptions) do
      add :callback_url, :string, null: false
      add :after_event_id, :bigint, null: false, default: 0
      add :last_delivered_event_id, :bigint, null: false, default: 0

      timestamps()
    end

    create unique_index(:subscriptions, :callback_url)

    create constraint(:subscriptions, :check_dom_callback_url, check: "callback_url <> ''")

    create constraint(:subscriptions, :check_dom_after_event_id, check: "after_event_id >= 0")

    create constraint(:subscriptions, :check_dom_last_delivered_event_id,
             check: "last_delivered_event_id >= after_event_id"
           )
  end
end
