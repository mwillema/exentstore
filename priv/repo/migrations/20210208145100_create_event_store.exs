defmodule ExentStore.Repo.Migrations.CreateEventStore do
  use Ecto.Migration

  def change do
    create table(:event_store, primary_key: false) do
      add :event_id, :bigserial, primary_key: true
      add :event_type, :string, null: false
      add :event_body, :binary, null: false
      add :occurred_on, :naive_datetime, null: false
      add :stream_name, :string
      add :stream_version, :integer
    end

    create unique_index(:event_store, [:stream_name, :stream_version])

    create constraint(:event_store, :check_dom_event_type, check: "event_type <> ''")

    create constraint(:event_store, :check_coex_stream,
             check: "(stream_name is not null and stream_version is not null)
    or (stream_name is null and stream_version is null)"
           )

    create constraint(:event_store, :check_dom_stream_name, check: "stream_name <> ''")

    create constraint(:event_store, :check_dom_stream_version, check: "stream_version > 0")
  end
end
