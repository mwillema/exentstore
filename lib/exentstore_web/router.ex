defmodule ExentStoreWeb.Router do
  use ExentStoreWeb, :router

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_live_flash
    plug :put_root_layout, {ExentStoreWeb.LayoutView, :root}
    plug :protect_from_forgery
    plug :put_secure_browser_headers
  end

  pipeline :api do
    plug :accepts, ["json"]
  end

  scope "/", ExentStoreWeb do
    pipe_through :browser

    live "/", PageLive, :index
  end

  scope "/api", ExentStoreWeb do
    pipe_through :api

    resources "/streams/:stream_name", EventStreamController, only: [:create]

    resources "/streams", EventStreamController,
      param: "stream_name",
      only: [:show, :delete]

    head "/streams/:stream_name", EventStreamController, :show

    get "/health", HealthController, :health

    resources "/subscriptions", SubscriptionController, only: [:create]
  end

  # Enables LiveDashboard only for development
  #
  # If you want to use the LiveDashboard in production, you should put
  # it behind authentication and allow only admins to access it.
  # If your application does not have an admins-only section yet,
  # you can use Plug.BasicAuth to set up some basic authentication
  # as long as you are also using SSL (which you should anyway).
  if Mix.env() in [:dev, :test] do
    import Phoenix.LiveDashboard.Router

    scope "/" do
      pipe_through :browser
      live_dashboard "/dashboard", metrics: ExentStoreWeb.Telemetry
    end
  end
end
