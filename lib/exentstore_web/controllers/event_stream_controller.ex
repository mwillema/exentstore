defmodule ExentStoreWeb.EventStreamController do
  use ExentStoreWeb, :controller

  alias ExentStore.StorageEngine.{
    AppendToStream,
    DeleteEventStream,
    EventStreamExists,
    LoadEventStream
  }

  action_fallback ExentStoreWeb.FallbackController

  def create(conn, params) do
    with {:ok, command} <- AppendToStream.new(params),
         :ok <- AppendToStream.execute(command) do
      conn
      |> send_resp(:created, "")
    end
  end

  def show(%{adapter: {_, %{method: "HEAD"}}} = conn, params) do
    with {:ok, command} <- EventStreamExists.new(params),
         stream_exists? <- EventStreamExists.execute(command) do
      case stream_exists? do
        true -> conn |> send_resp(:ok, "")
        _ -> conn |> send_resp(:not_found, "")
      end
    end
  end

  def show(conn, params) do
    with {:ok, command} <- LoadEventStream.new(params),
         {stream_version, events} <- LoadEventStream.execute(command) do
      case stream_version do
        0 ->
          {:error, :not_found}

        _ ->
          conn
          |> render("show.json", stream_version: stream_version, events: events)
      end
    end
  end

  def delete(conn, params) do
    with {:ok, command} <- DeleteEventStream.new(params),
         existed? <- DeleteEventStream.execute(command) do
      case existed? do
        true -> conn |> send_resp(:no_content, "")
        _ -> {:error, :not_found}
      end
    end
  end
end
