defmodule ExentStoreWeb.SubscriptionController do
  use ExentStoreWeb, :controller

  alias ExentStore.SubscriptionManager.{SubscribeToAll, Subscription}

  action_fallback ExentStoreWeb.FallbackController

  def create(conn, params) do
    with {:ok, command} <- SubscribeToAll.new(params),
         %Subscription{} = subscription <- SubscribeToAll.execute(command) do
      conn
      |> put_status(:created)
      |> render("show.json", subscription: subscription)
    end
  end
end
