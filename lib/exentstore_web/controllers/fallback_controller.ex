defmodule ExentStoreWeb.FallbackController do
  @moduledoc """
  Translates controller action results into valid `Plug.Conn` responses.

  See `Phoenix.Controller.action_fallback/1` for more details.
  """
  use ExentStoreWeb, :controller

  def call(conn, {:error, %Ecto.Changeset{} = changeset}) do
    conn
    |> put_status(:unprocessable_entity)
    |> put_view(ExentStoreWeb.ChangesetView)
    |> render("error.json", changeset: changeset)
  end

  def call(conn, {:error, :not_found}) do
    conn
    |> put_status(:not_found)
    |> put_view(ExentStoreWeb.ErrorView)
    |> render(:"404")
  end

  def call(conn, {:error, :unauthorized}) do
    conn
    |> put_status(:unauthorized)
    |> put_view(ExentStoreWeb.ErrorView)
    |> render(:"401")
  end

  def call(conn, {:error, :wrong_expected_version, version}) do
    conn
    |> put_status(:unprocessable_entity)
    |> put_view(ExentStoreWeb.ErrorView)
    |> render("wrong_expected_version.json", version: version)
  end
end
