defmodule ExentStoreWeb.HealthController do
  use ExentStoreWeb, :controller

  alias ExentStore.Health

  action_fallback ExentStoreWeb.FallbackController

  def health(conn, _params) do
    case Health.check_database() do
      :ok ->
        conn
        |> render("success.json", status: "UP")

      _ ->
        conn
        |> put_status(:service_unavailable)
        |> render("error.json", status: "DOWN", reason: "Unreachable database")
    end
  end
end
