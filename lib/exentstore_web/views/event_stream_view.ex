defmodule ExentStoreWeb.EventStreamView do
  use ExentStoreWeb, :view

  alias ExentStoreWeb.EventView

  def render("show.json", %{stream_version: stream_version, events: events}) do
    %{stream_version: stream_version, events: render_many(events, EventView, "event.json")}
  end
end
