defmodule ExentStoreWeb.HealthView do
  use ExentStoreWeb, :view

  def render("success.json", %{status: status}) do
    %{
      status: status
    }
  end

  def render("error.json", %{status: status, reason: reason}) do
    %{
      status: status,
      reason: reason
    }
  end
end
