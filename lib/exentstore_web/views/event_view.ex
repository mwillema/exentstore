defmodule ExentStoreWeb.EventView do
  use ExentStoreWeb, :view

  def render("event.json", %{event: event}) do
    %{
      event_id: event.event_id,
      event_type: event.event_type,
      event_body: event.event_body,
      occurred_on: event.occurred_on,
      stream_name: event.stream_name,
      stream_version: event.stream_version
    }
  end
end
