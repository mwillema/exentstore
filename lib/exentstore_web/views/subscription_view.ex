defmodule ExentStoreWeb.SubscriptionView do
  use ExentStoreWeb, :view

  def render("show.json", %{subscription: subscription}) do
    %{
      subscription_id: subscription.id,
      after_event_id: subscription.after_event_id,
      callback_url: subscription.callback_url
    }
  end
end
