defmodule ExentStore.StorageEngine do
  @moduledoc """
  The StorageEngine context represents a storage engine for events.
  """

  import Ecto.Query, warn: false

  alias ExentStore.{EventPublisher, Repo}

  alias ExentStore.StorageEngine.StoredEvent

  #
  # READING EVENTS
  #

  @doc """
  Returns the number of stored events.

  ## Examples

      iex> ExentStore.StorageEngine.count_stored_events()
      0

  """
  def count_stored_events do
    Repo.aggregate(StoredEvent, :count)
  end

  @doc """
  Returns all the stored events after a given ID.

  ## Effects

  Returns the stored events `evts` such that for all `e` in `evts`,
  `e.event_id` > `event_id` and all `e` are sorted by their ID in ascending order.

  ## Examples

      iex> ExentStore.StorageEngine.all_stored_events_after(0)
      []

  """
  def all_stored_events_after(event_id) when is_integer(event_id) do
    event_id
    |> StoredEvent.all_stored_events_after_query()
    |> Repo.all()
  end

  @doc """
  Returns true if a given event stream exists, else returns false.

  ## Requires

  - `stream_name` is string

  ## Effects

  Returns true iff an event stream named `stream_name` exists.

  ## Examples

      iex> ExentStore.StorageEngine.event_stream_exists?("MyStream")
      false

  """
  def event_stream_exists?(stream_name) when is_binary(stream_name) do
    stream_name
    |> StoredEvent.stream_exists_query()
    |> Repo.exists?()
  end

  @doc """
  Returns an event stream after a given version.

  ## Requires

  - `stream_name` is a string
  - `after_stream_version` >= 0

  ## Effects

  Returns the event stream named `stream_name` starting from `after_stream_version` + 1
  as `{stream_version, events}` where:

  - `stream_version` is the version of the loaded stream
  - `events` are the events of the stream sorted by their position in the stream in ascending order.

  ## Examples

      iex> ExentStore.StorageEngine.load_event_stream("MyStream", 0)
      {0, []}
  """
  def load_event_stream(stream_name, after_stream_version)
      when is_binary(stream_name) and is_integer(after_stream_version) and
             after_stream_version >= 0 do
    events =
      StoredEvent.load_event_stream_query(stream_name, after_stream_version)
      |> Repo.all()

    # Getting the last element of a list is in O(n). So the choice has been made to perform
    # a DB query instead. Is it really faster? Time (or load testing) will tell :-)
    stream_version = get_stream_version(stream_name)

    {stream_version, events}
  end

  #
  # WRITING EVENTS
  #

  @doc """
  Appends a list of new events to a stream.

  ## Requires

  - `stream_name` not empty
  - `expected_version` >= 0
  - `events` is a list of `ExentStore.StorageEngine.Event`

  ## Effects

  If the stream `stream_name` does not exist, `expected_version` must be 0.

  Otherwise `expected_version` is compared to the `actual_version` of the stream `stream_name`.
  This will yield one of three results:

   - `expected_version` > `actual_version`:
        returns `{:error, :wrong_expected_version, actual_version}`
   - `expected_version` = `actual_version`:
        appends the list of events `events` to the stream named `stream_name` and returns `:ok`
   - `expected_version` < `actual_version`:
        returns `{:error, :wrong_expected_version, actual_version, actual_events}`
        where `actual_events` are the events of the stream after `expected_version`,
        i.e., the events that were concurrently appended to the stream.

  ## Examples

      iex> ExentStore.StorageEngine.append_to_stream("MyStream", 0, [%ExentStore.StorageEngine.Event{event_type: "MyEvent", event_body: <<0>>}])
      :ok

      iex> ExentStore.StorageEngine.append_to_stream("MyStream", 1, [%ExentStore.StorageEngine.Event{event_type: "MyEvent", event_body: <<0>>}])
      {:error, :wrong_expected_version, 0}

  """
  def append_to_stream(stream_name, expected_version, events)
      when is_binary(stream_name) and stream_name != "" and expected_version >= 0 and
             is_list(events) do
    with :ok <- check_version(stream_name, expected_version) do
      events
      |> StoredEvent.to_stored_events(stream_name, expected_version)
      |> append_to(stream_name)
    end
  end

  @doc """
  Deletes an event stream.

  ## Requires

  - `stream_name` not empty

  ## Effects

  Deletes the event stream named `stream_name` and
  returns true if the stream existed, else returns false.

  ## Examples

      iex> ExentStore.StorageEngine.delete_event_stream("MyStream")
      false
  """
  def delete_event_stream(stream_name) do
    {count, _} =
      stream_name
      |> StoredEvent.delete_event_stream_query()
      |> Repo.delete_all()

    count > 0
  end

  #
  # HELPERS
  #

  defp check_version(_stream_name, 0 = _stream_version), do: :ok

  defp check_version(stream_name, stream_version) do
    case stream_exists?(stream_name, stream_version) do
      true -> :ok
      _ -> {:error, :wrong_expected_version, get_stream_version(stream_name)}
    end
  end

  defp stream_exists?(stream_name, stream_version) do
    StoredEvent.stream_exists_query(stream_name, stream_version)
    |> Repo.exists?()
  end

  defp append_to(events, stream_name) do
    Repo.insert_all(StoredEvent, events)

    EventPublisher.publish(:events_appended)

    :ok
  catch
    :error,
    %Postgrex.Error{postgres: %{constraint: "event_store_stream_name_stream_version_index"}} ->
      {:error, :wrong_expected_version, get_stream_version(stream_name)}
  end

  defp get_stream_version(stream_name) do
    case get_stream_version_or_nil(stream_name) do
      nil -> 0
      stream_version -> stream_version
    end
  end

  defp get_stream_version_or_nil(stream_name) do
    stream_name
    |> StoredEvent.stream_version_query()
    |> Repo.one()
  end
end
