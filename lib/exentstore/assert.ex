defmodule ExentStore.Assert do
  @moduledoc """
  Provides assertion functions.
  """

  @doc """
  Asserts the given expression is true. If not it raises an exception.
  """
  def is_true(expression) do
    if !expression do
      raise "assertion broken"
    end
  end
end
