defmodule ExentStore.Health do
  @moduledoc """
  The Health context allows one to check the status of the running application.
  """

  alias Ecto.Adapters.SQL
  alias ExentStore.Repo

  @doc """
  Checks that the underlying database is up.

  ## Effects

  Returns `:ok` if the database is up. Otherwise returns `:error`.

  ## Examples

      iex> ExentStore.Health.check_database()
      :ok
  """
  def check_database do
    SQL.query!(Repo, "select 1", [])
    :ok
  rescue
    DBConnection.ConnectionError -> :error
  end
end
