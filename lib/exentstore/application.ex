defmodule ExentStore.Application do
  # See https://hexdocs.pm/elixir/Application.html
  # for more information on OTP Applications
  @moduledoc false

  use Application

  def start(_type, _args) do
    children = [
      # Start the Ecto repository
      ExentStore.Repo,
      # Start the Telemetry supervisor
      ExentStoreWeb.Telemetry,
      # Start the PubSub system
      {Phoenix.PubSub, name: ExentStore.PubSub},
      # Start the Endpoint (http/https)
      ExentStoreWeb.Endpoint
      # Start a worker by calling: ExentStore.Worker.start_link(arg)
      # {ExentStore.Worker, arg}
    ]

    children =
      case Application.get_env(:exentstore, :env) do
        :test ->
          children

        _ ->
          children ++
            [
              {DynamicSupervisor,
               name: ExentStore.SubscriptionManager.EventNotifierSupervisor,
               strategy: :one_for_one},
              {ExentStore.SubscriptionManager.EventNotifierRegistry,
               name: ExentStore.SubscriptionManager.EventNotifierRegistry}
            ]
      end

    # See https://hexdocs.pm/elixir/Supervisor.html
    # for other strategies and supported options
    opts = [strategy: :one_for_one, name: ExentStore.Supervisor]
    Supervisor.start_link(children, opts)
  end

  # Tell Phoenix to update the endpoint configuration
  # whenever the application is updated.
  def config_change(changed, _new, removed) do
    ExentStoreWeb.Endpoint.config_change(changed, removed)
    :ok
  end
end
