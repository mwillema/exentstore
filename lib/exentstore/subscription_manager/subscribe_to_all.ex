defmodule ExentStore.SubscriptionManager.SubscribeToAll do
  use Ecto.Schema
  import Ecto.Changeset

  alias __MODULE__

  alias ExentStore.{Repo, SubscriptionManager}

  alias ExentStore.SubscriptionManager.Subscription

  @primary_key false

  embedded_schema do
    field :after_event_id, :integer
    field :callback_url, :string
  end

  def new(attrs) do
    changeset = changeset(%SubscribeToAll{}, attrs)

    if changeset.valid? do
      {:ok, Ecto.Changeset.apply_changes(changeset)}
    else
      changeset = %{changeset | action: :subscribe}
      {:error, changeset}
    end
  end

  def execute(%SubscribeToAll{} = command) do
    SubscriptionManager.subscribe_to_all!(command.after_event_id, command.callback_url)
  end

  defp changeset(command, attrs) do
    command
    |> cast(attrs, [:after_event_id, :callback_url])
    |> validate_required([:after_event_id, :callback_url])
    |> validate_number(:after_event_id, greater_than_or_equal_to: 0)
    |> validate_unique_callback_url()
  end

  defp validate_unique_callback_url(changeset) do
    validate_change(changeset, :callback_url, fn :callback_url, callback_url ->
      if exists?(callback_url) do
        [callback_url: "must be unique"]
      else
        []
      end
    end)
  end

  defp exists?(callback_url) do
    callback_url
    |> Subscription.exists_query()
    |> Repo.exists?()
  end
end
