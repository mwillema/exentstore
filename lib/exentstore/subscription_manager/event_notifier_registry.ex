defmodule ExentStore.SubscriptionManager.EventNotifierRegistry do
  use GenServer

  alias ExentStore.{EventPublisher, SubscriptionManager}

  alias ExentStore.SubscriptionManager.{
    EventNotifier,
    EventNotifierSupervisor,
    Subscription
  }

  #
  # Client API
  #

  @doc """
  Starts the registry.
  """
  def start_link(opts) do
    GenServer.start_link(__MODULE__, :ok, opts)
  end

  #
  # Server (callbacks)
  #

  @doc """
  Initializes event notifiers for the existing subscriptions.
  """
  @impl true
  def init(:ok) do
    EventPublisher.subscribe()

    state =
      SubscriptionManager.list_subscriptions()
      |> Enum.reduce({%{}, %{}}, &init_notifier/2)

    {:ok, state}
  end

  @doc """
  Initializes a new event notifier for the given subscription.
  """
  @impl true
  def handle_info({:created, %Subscription{} = subscription}, state) do
    {:noreply, init_notifier(subscription, state)}
  end

  @impl true
  def handle_info({:DOWN, ref, :process, _pid, _reason}, {subscription_ids, refs}) do
    {subscription_id, refs} = Map.pop(refs, ref)
    subscription_ids = Map.delete(subscription_ids, subscription_id)
    {:noreply, {subscription_ids, refs}}
  end

  @impl true
  def handle_info(_msg, state) do
    {:noreply, state}
  end

  #
  # HELPER FUNCTIONS
  #

  defp init_notifier(subscription, {subscription_ids, refs}) do
    subscription_id = subscription.id

    if Map.has_key?(subscription_ids, subscription_id) do
      {subscription_ids, refs}
    else
      {:ok, pid} =
        DynamicSupervisor.start_child(
          EventNotifierSupervisor,
          {EventNotifier, {subscription}}
        )

      ref = Process.monitor(pid)

      refs = Map.put(refs, ref, subscription_id)
      subscription_ids = Map.put(subscription_ids, subscription_id, pid)

      {subscription_ids, refs}
    end
  end
end
