defmodule ExentStore.SubscriptionManager.Subscription do
  @moduledoc """
  A subscription.

  ## Specfields

  - `callback_url` : string             // The URL to notify.
  - `after_event_id` : integer          // The event ID after which the subscription starts.
  - `last_delivered_event_id` : integer // The ID of the last delivered event.

  ## Invariants

  - `callback_url` is a unique URL
  - `after_event_id` >= 0
  - `last_delivered_event_id` >= `after_event_id`

  """
  use Ecto.Schema
  import Ecto.Query, warn: false

  alias __MODULE__

  schema "subscriptions" do
    field :callback_url, :string
    field :after_event_id, :integer
    field :last_delivered_event_id, :integer

    timestamps()
  end

  @doc false
  def exists_query(callback_url) do
    from(s in Subscription, where: s.callback_url == ^callback_url)
  end
end
