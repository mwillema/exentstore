defmodule ExentStore.SubscriptionManager.EventNotifier do
  use GenServer, restart: :temporary

  require Logger

  alias ExentStore.{EventPublisher, StorageEngine, SubscriptionManager}

  alias ExentStore.SubscriptionManager.{
    Subscription
  }

  @http_client Application.compile_env(:exentstore, :http_client)

  #
  # Client API
  #

  @doc """
  Starts a new event notifier for the given subscription.
  """
  def start_link({%Subscription{} = subscription}) do
    GenServer.start_link(__MODULE__, {subscription})
  end

  #
  # Server (callbacks)
  #

  @impl true
  def init({subscription}) do
    Logger.debug("Initializing event notifier [#{subscription.id}]")

    EventPublisher.subscribe()

    {:ok, %{subscription: subscription}}
  end

  @impl true
  def handle_info({:events_appended}, %{subscription: subscription}) do
    stored_events = StorageEngine.all_stored_events_after(subscription.last_delivered_event_id)

    @http_client.post(subscription.callback_url, stored_events)

    last_delivered_event_id =
      stored_events
      |> List.last()
      |> Map.fetch!(:event_id)

    subscription =
      SubscriptionManager.save_last_delivered_event_id!(subscription.id, last_delivered_event_id)

    {:noreply, %{subscription: subscription}}
  end

  @impl true
  def handle_info(_, state) do
    {:noreply, state}
  end
end
