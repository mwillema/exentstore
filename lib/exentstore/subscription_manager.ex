defmodule ExentStore.SubscriptionManager do
  @moduledoc """
  The SubscriptionManager context.
  """

  import Ecto.Query, warn: false

  alias ExentStore.{Assert, EventPublisher, Repo}

  alias ExentStore.SubscriptionManager.Subscription

  @doc """
  Subscribes to all events after a given event ID (catch-up subscription).

  ## Requires

  - `after_event_id` >= 0
  - `callback_url` is a valid URL
  - there's no existing subscription with `callback_url`

  ## Effects

  Returns the new Subscription and notifies (as POST requests) the URL `callback_url` of all events after the event ID `after_event_id`.

  ## Examples

      iex> subscription = ExentStore.SubscriptionManager.subscribe_to_all!(0, "http://iam")
      ...> with %Subscription{callback_url: "http://iam", after_event_id: 0} <- subscription, do: :passed

  """
  def subscribe_to_all!(after_event_id, callback_url)
      when is_integer(after_event_id) and after_event_id >= 0 and is_binary(callback_url) do
    subscription =
      %Subscription{}
      |> Ecto.Changeset.change(%{
        callback_url: callback_url,
        after_event_id: after_event_id,
        last_delivered_event_id: after_event_id
      })
      |> Repo.insert!()

    EventPublisher.publish(:created, subscription)

    subscription
  end

  @doc """
  Saves the last delivered event id of a given subscription.

  ## Requires

  There exists a Subscription `s` with `s.id` = `subscription_id` and
  `s.last_delivered_event_id` < `last_delivered_event_id`

  ## Effects

  Sets `s.last_delivered_event_id` = `last_delivered_event_id` where `s.id` = `subscription_id`
  and returns `s`.
  """
  def save_last_delivered_event_id!(subscription_id, last_delivered_event_id)
      when is_integer(subscription_id) and is_integer(last_delivered_event_id) do
    subscription = Repo.get!(Subscription, subscription_id)

    Assert.is_true(subscription.last_delivered_event_id < last_delivered_event_id)

    subscription
    |> Ecto.Changeset.change(%{
      last_delivered_event_id: last_delivered_event_id
    })
    |> Repo.update!()
  end

  @doc """
  Lists the existing subscriptions.

  ## Effects

  Returns the list of the existing subscriptions sorted by their ID in ascending order.

  ## Examples

      iex> ExentStore.SubscriptionManager.list_subscriptions()
      []

  """
  def list_subscriptions do
    from(s in Subscription, order_by: s.id)
    |> Repo.all()
  end
end
