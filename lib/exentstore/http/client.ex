defmodule ExentStore.Http.Client do
  @callback post(url :: String.t(), body :: any()) :: any
end
