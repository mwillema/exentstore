defmodule ExentStore.Http.HttpoisonClient do
  alias ExentStore.Http.Client

  @behaviour Client
  def post(url, body) do
    json_body = Jason.encode!(body)

    HTTPoison.post!(url, json_body)
  end
end
