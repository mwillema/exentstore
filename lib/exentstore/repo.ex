defmodule ExentStore.Repo do
  use Ecto.Repo,
    otp_app: :exentstore,
    adapter: Ecto.Adapters.Postgres
end
