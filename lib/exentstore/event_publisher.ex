defmodule ExentStore.EventPublisher do
  alias Phoenix.PubSub

  @pubsub ExentStore.PubSub

  @actions ~w(
    created
  )a

  def publish(:events_appended = action) do
    PubSub.broadcast!(@pubsub, topic(), {action})
  end

  def publish(action, %_{} = object) when action in @actions do
    PubSub.broadcast!(@pubsub, topic(), {action, object})
  end

  def publish(:updated = action, previous, current) do
    PubSub.broadcast(@pubsub, topic(), {action, previous, current})
  end

  def subscribe do
    PubSub.subscribe(@pubsub, topic())
  end

  def topic do
    Atom.to_string(__MODULE__)
  end
end
