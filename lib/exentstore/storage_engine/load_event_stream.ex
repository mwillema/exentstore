defmodule ExentStore.StorageEngine.LoadEventStream do
  use Ecto.Schema
  import Ecto.Changeset

  alias __MODULE__

  alias ExentStore.StorageEngine

  @primary_key false

  embedded_schema do
    field :stream_name, :string
    field :after_stream_version, :integer
  end

  def new(attrs) do
    changeset = changeset(%LoadEventStream{}, attrs)

    if changeset.valid? do
      {:ok, Ecto.Changeset.apply_changes(changeset)}
    else
      changeset = %{changeset | action: :load}
      {:error, changeset}
    end
  end

  def execute(%LoadEventStream{} = command) do
    StorageEngine.load_event_stream(command.stream_name, command.after_stream_version)
  end

  defp changeset(command, attrs) do
    command
    |> cast(attrs, [:stream_name, :after_stream_version])
    |> validate_required([:stream_name, :after_stream_version])
    |> validate_number(:after_stream_version, greater_than_or_equal_to: 0)
  end
end
