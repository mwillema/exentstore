defmodule ExentStore.StorageEngine.Event do
  use Ecto.Schema
  import Ecto.Changeset

  alias __MODULE__

  @primary_key false

  @attrs ~w(event_type event_body)a

  embedded_schema do
    field :event_type, :string
    field :event_body, :binary
  end

  def new(%{} = attrs) do
    changeset = changeset(%Event{}, attrs)

    if changeset.valid? do
      {:ok, Ecto.Changeset.apply_changes(changeset)}
    else
      changeset = %{changeset | action: :append}
      {:error, changeset}
    end
  end

  @doc false
  def changeset(event, attrs) do
    event
    |> cast(attrs, @attrs)
    |> validate_required(@attrs)
  end
end
