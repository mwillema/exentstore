defmodule ExentStore.StorageEngine.AppendToStream do
  use Ecto.Schema
  import Ecto.Changeset

  alias __MODULE__

  alias ExentStore.StorageEngine
  alias ExentStore.StorageEngine.Event

  @primary_key false

  embedded_schema do
    field :stream_name, :string
    field :expected_version, :integer
    embeds_many :events, Event
  end

  def new(attrs) do
    changeset = changeset(%AppendToStream{}, attrs)

    if changeset.valid? do
      {:ok, Ecto.Changeset.apply_changes(changeset)}
    else
      changeset = %{changeset | action: :append}
      {:error, changeset}
    end
  end

  def execute(%AppendToStream{} = command) do
    StorageEngine.append_to_stream(command.stream_name, command.expected_version, command.events)
  end

  defp changeset(command, attrs) do
    command
    |> cast(attrs, [:stream_name, :expected_version])
    |> cast_embed(:events, required: true)
    |> validate_required([:stream_name, :expected_version])
    |> validate_number(:expected_version, greater_than_or_equal_to: 0)
  end
end
