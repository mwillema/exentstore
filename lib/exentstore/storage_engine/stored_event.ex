defmodule ExentStore.StorageEngine.StoredEvent do
  @moduledoc """
  A stored event.

  ## Specfields

  - `event_id` : integer       // The unique ID of the stored event.
  - `event_type` : string      // The type name of the stored event.
  - `event_body` : bytes       // The serialized body of the stored event.
  - `occurred_on` : timestamp  // The date and time of the occurrence of the stored event.
  - `stream_name` : string     // The name of the event stream the stored event belongs to.
  - `stream_version` : integer // The number of the stored event in the steram.

  ## Invariants

  TBD

  """
  use Ecto.Schema
  import Ecto.Changeset
  import Ecto.Query, warn: false

  alias __MODULE__

  @primary_key {:event_id, :id, autogenerate: true}
  @derive {Phoenix.Param, key: :event_id}
  @derive {Jason.Encoder,
           only: [
             :event_id,
             :event_type,
             :event_body,
             :occurred_on,
             :stream_name,
             :stream_version
           ]}

  @required_attrs ~w(event_type event_body)a
  @optional_attrs ~w(stream_name stream_version)a
  @allowed_attrs @required_attrs ++ @optional_attrs

  schema "event_store" do
    field :event_type, :string
    field :event_body, :binary
    field :occurred_on, :naive_datetime
    field :stream_name, :string
    field :stream_version, :integer
  end

  @doc false
  def changeset(stored_event, attrs) do
    stored_event
    |> cast(attrs, @allowed_attrs)
    |> validate_required(@required_attrs)
  end

  @doc false
  def all_stored_events_after_query(event_id) do
    from(e in StoredEvent, where: e.event_id > ^event_id, order_by: e.event_id)
  end

  @doc false
  def stream_exists_query(stream_name) do
    from(e in StoredEvent, where: e.stream_name == ^stream_name)
  end

  @doc false
  def stream_exists_query(stream_name, stream_version) do
    from(e in StoredEvent,
      where: e.stream_name == ^stream_name and e.stream_version == ^stream_version
    )
  end

  @doc false
  def stream_version_query(stream_name) do
    from(e in StoredEvent,
      where: e.stream_name == ^stream_name,
      order_by: [desc: :stream_version],
      limit: 1,
      select: e.stream_version
    )
  end

  @doc false
  def load_event_stream_query(stream_name, after_stream_version) do
    from(e in StoredEvent,
      where: e.stream_name == ^stream_name and e.stream_version > ^after_stream_version,
      order_by: :event_id
    )
  end

  @doc false
  def delete_event_stream_query(stream_name) do
    from(e in StoredEvent,
      where: e.stream_name == ^stream_name
    )
  end

  @doc false
  def to_stored_events(events, stream_name, expected_version) do
    occurred_on = now()

    {events, _} =
      events
      |> Enum.map(&Map.from_struct/1)
      |> Enum.map_reduce(expected_version + 1, fn event, stream_version ->
        {
          event
          |> Map.put(:occurred_on, occurred_on)
          |> Map.put(:stream_name, stream_name)
          |> Map.put(:stream_version, stream_version),
          stream_version + 1
        }
      end)

    events
  end

  defp now do
    # Ecto does not support milliseconds in timestamps
    NaiveDateTime.utc_now() |> NaiveDateTime.truncate(:second)
  end
end
