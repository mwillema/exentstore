defmodule ExentStore.StorageEngine.DeleteEventStream do
  use Ecto.Schema
  import Ecto.Changeset

  alias __MODULE__

  alias ExentStore.StorageEngine

  @primary_key false

  embedded_schema do
    field :stream_name, :string
  end

  def new(attrs) do
    changeset = changeset(%DeleteEventStream{}, attrs)

    if changeset.valid? do
      {:ok, Ecto.Changeset.apply_changes(changeset)}
    else
      changeset = %{changeset | action: :delete}
      {:error, changeset}
    end
  end

  def execute(%DeleteEventStream{} = command) do
    StorageEngine.delete_event_stream(command.stream_name)
  end

  defp changeset(command, attrs) do
    command
    |> cast(attrs, [:stream_name])
    |> validate_required([:stream_name])
  end
end
