defmodule ExentStore.StorageEngine.AppendToStreamTest do
  use ExUnit.Case
  import ExentStore.DataCase, only: [errors_on: 1]

  alias ExentStore.StorageEngine.AppendToStream
  alias ExentStore.StorageEngine.Event

  describe "new/1" do
    test "with valid data returns a new command" do
      attrs = %{
        stream_name: "MyStream",
        expected_version: 2,
        events: [
          %{event_type: "A", event_body: <<0>>},
          %{event_type: "B", event_body: <<1>>}
        ]
      }

      assert {:ok,
              %AppendToStream{
                stream_name: "MyStream",
                expected_version: 2,
                events: [
                  %Event{event_type: "A", event_body: <<0>>},
                  %Event{event_type: "B", event_body: <<1>>}
                ]
              }} = AppendToStream.new(attrs)
    end

    test "with missing data returns an error changeset" do
      attrs = %{
        stream_name: " ",
        events: []
      }

      assert {:error, %Ecto.Changeset{action: :append} = changeset} = AppendToStream.new(attrs)
      assert %{stream_name: ["can't be blank"]} = errors_on(changeset)
      assert %{expected_version: ["can't be blank"]} = errors_on(changeset)
      assert %{events: ["can't be blank"]} = errors_on(changeset)
    end

    test "with negative expected version returns an error changeset" do
      attrs = %{
        stream_name: "MyStream",
        expected_version: -1
      }

      assert {:error, %Ecto.Changeset{} = changeset} = AppendToStream.new(attrs)
      assert %{expected_version: ["must be greater than or equal to 0"]} = errors_on(changeset)
    end

    test "with invalid events returns an error changeset" do
      attrs = %{
        stream_name: "MyStream",
        expected_version: 0,
        events: [%{event_type: "A"}, %{event_body: <<0>>}]
      }

      assert {:error, %Ecto.Changeset{} = changeset} = AppendToStream.new(attrs)

      assert %{events: [%{event_body: ["can't be blank"]}, %{event_type: ["can't be blank"]}]} =
               errors_on(changeset)
    end
  end
end
