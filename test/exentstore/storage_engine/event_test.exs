defmodule ExentStore.StorageEngine.EventTest do
  use ExUnit.Case
  import ExentStore.DataCase, only: [errors_on: 1]

  alias ExentStore.StorageEngine.Event

  describe "new/1" do
    test "with valid data returns a new event" do
      attrs = %{
        event_type: "A",
        event_body: <<0>>
      }

      assert {:ok, %Event{event_type: "A", event_body: <<0>>}} = Event.new(attrs)
    end

    test "with missing data returns an error changeset" do
      attrs = %{
        event_type: " "
      }

      assert {:error, %Ecto.Changeset{} = changeset} = Event.new(attrs)
      assert %{event_type: ["can't be blank"]} = errors_on(changeset)
      assert %{event_body: ["can't be blank"]} = errors_on(changeset)
    end
  end
end
