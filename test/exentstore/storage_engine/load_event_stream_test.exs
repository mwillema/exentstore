defmodule ExentStore.StorageEngine.LoadEventStreamTest do
  use ExUnit.Case
  import ExentStore.DataCase, only: [errors_on: 1]

  alias ExentStore.StorageEngine.LoadEventStream

  describe "new/1" do
    test "with valid data returns a new command" do
      attrs = %{
        stream_name: "MyStream",
        after_stream_version: 5
      }

      assert {:ok,
              %LoadEventStream{
                stream_name: "MyStream",
                after_stream_version: 5
              }} = LoadEventStream.new(attrs)
    end

    test "with missing data returns an error changeset" do
      attrs = %{
        stream_name: " "
      }

      assert {:error, %Ecto.Changeset{action: :load} = changeset} = LoadEventStream.new(attrs)
      assert %{stream_name: ["can't be blank"]} = errors_on(changeset)
      assert %{after_stream_version: ["can't be blank"]} = errors_on(changeset)
    end

    test "with negative stream version returns an error changeset" do
      attrs = %{
        stream_name: "MyStream",
        after_stream_version: -1
      }

      assert {:error, %Ecto.Changeset{} = changeset} = LoadEventStream.new(attrs)

      assert %{after_stream_version: ["must be greater than or equal to 0"]} =
               errors_on(changeset)
    end
  end
end
