defmodule ExentStore.StorageEngine.EventStreamExistsTest do
  use ExUnit.Case
  import ExentStore.DataCase, only: [errors_on: 1]

  alias ExentStore.StorageEngine.EventStreamExists

  describe "new/1" do
    test "with valid data returns a new command" do
      attrs = %{stream_name: "MyStream"}

      assert {:ok, %EventStreamExists{stream_name: "MyStream"}} = EventStreamExists.new(attrs)
    end

    test "with missing data returns an error changeset" do
      assert {:error, %Ecto.Changeset{action: :exists} = changeset} = EventStreamExists.new(%{})
      assert %{stream_name: ["can't be blank"]} = errors_on(changeset)
    end
  end
end
