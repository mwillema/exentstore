defmodule ExentStore.StorageEngine.DeleteEventStreamTest do
  use ExUnit.Case
  import ExentStore.DataCase, only: [errors_on: 1]

  alias ExentStore.StorageEngine.DeleteEventStream

  describe "new/1" do
    test "with valid data returns a new command" do
      attrs = %{stream_name: "MyStream"}

      assert {:ok, %DeleteEventStream{stream_name: "MyStream"}} = DeleteEventStream.new(attrs)
    end

    test "with missing data returns an error changeset" do
      assert {:error, %Ecto.Changeset{action: :delete} = changeset} = DeleteEventStream.new(%{})
      assert %{stream_name: ["can't be blank"]} = errors_on(changeset)
    end
  end
end
