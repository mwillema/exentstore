defmodule ExentStore.SubscriptionManagerTest do
  use ExentStore.DataCase

  alias ExentStore.{EventPublisher, SubscriptionManager}
  alias ExentStore.SubscriptionManager.Subscription

  doctest SubscriptionManager

  describe "subscribe_to_all/1" do
    test "subscribes to all events" do
      EventPublisher.subscribe()

      after_event_id = 0
      callback_url = "http://iam"

      assert (%Subscription{
                callback_url: ^callback_url,
                after_event_id: ^after_event_id,
                last_delivered_event_id: ^after_event_id
              } = subscription) =
               SubscriptionManager.subscribe_to_all!(after_event_id, callback_url)

      assert_receive {:created, ^subscription}
    end

    test "subscribes to all events after a given one" do
      EventPublisher.subscribe()

      after_event_id = 5
      callback_url = "http://iam"

      assert (%Subscription{
                callback_url: ^callback_url,
                after_event_id: ^after_event_id,
                last_delivered_event_id: ^after_event_id
              } = subscription) =
               SubscriptionManager.subscribe_to_all!(after_event_id, callback_url)

      assert_receive {:created, ^subscription}
    end

    test "subscribes to all events with non-unique callback URL" do
      after_event_id = 0
      callback_url = "http://iam"
      subscription_fixture(after_event_id, callback_url)

      EventPublisher.subscribe()

      assert_raise Ecto.ConstraintError, fn ->
        SubscriptionManager.subscribe_to_all!(after_event_id, callback_url)
      end

      refute_receive {:created, %Subscription{}}
    end
  end

  describe "save_last_delivered_event_id!/2" do
    test "saves the last delivered event ID" do
      %{id: subscription_id} = subscription_fixture(0, "http://iam")
      last_delivered_event_id = 1

      assert %Subscription{
               id: ^subscription_id,
               last_delivered_event_id: ^last_delivered_event_id
             } =
               SubscriptionManager.save_last_delivered_event_id!(
                 subscription_id,
                 last_delivered_event_id
               )
    end

    test "saves the last delivered event ID that is not greater than the current one" do
      last_delivered_event_id = 1
      %{id: subscription_id} = subscription_fixture(last_delivered_event_id, "http://iam")

      assert_raise RuntimeError, fn ->
        SubscriptionManager.save_last_delivered_event_id!(
          subscription_id,
          last_delivered_event_id
        )
      end
    end
  end

  describe "list_subscriptions/0" do
    test "returns all the subscriptions" do
      %{id: id1} = subscription_fixture(0, "http://iam1")
      %{id: id2} = subscription_fixture(0, "http://iam2")

      assert [%Subscription{id: ^id1}, %Subscription{id: ^id2}] =
               SubscriptionManager.list_subscriptions()
    end
  end
end
