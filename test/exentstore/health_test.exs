defmodule ExentStore.HealthTest do
  use ExentStore.DataCase

  alias ExentStore.Health

  doctest Health

  describe "check_database/0" do
    test "returns :ok when the database is up" do
      assert :ok = Health.check_database()
    end
  end
end
