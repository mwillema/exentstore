defmodule ExentStore.StorageEngineTest do
  use ExentStore.DataCase

  alias ExentStore.{EventPublisher, StorageEngine}
  alias ExentStore.StorageEngine.StoredEvent

  doctest StorageEngine

  describe "append_to_stream/3" do
    test "appends a new stream with one event" do
      stream_name = "A"
      expected_version = 0
      events = [event_fixture()]

      assert :ok = StorageEngine.append_to_stream(stream_name, expected_version, events)
      assert 1 = StorageEngine.count_stored_events()
    end

    test "appends a new stream with many events" do
      stream_name = "A"
      expected_version = 0
      events = events_fixture(3)

      assert :ok = StorageEngine.append_to_stream(stream_name, expected_version, events)
      assert 3 = StorageEngine.count_stored_events()
    end

    test "appends events to an existing stream" do
      stream_name = "A"
      expected_version = 2
      event_stream_fixture(stream_name, expected_version)
      events = events_fixture(3)

      assert :ok = StorageEngine.append_to_stream(stream_name, expected_version, events)
      assert 5 = StorageEngine.count_stored_events()
    end

    test "appends events to a stream with expected_version > actual_version" do
      stream_name = "A"
      actual_version = 2
      expected_version = actual_version + 1
      event_stream_fixture(stream_name, actual_version)
      events = events_fixture(1)

      assert {:error, :wrong_expected_version, ^actual_version} =
               StorageEngine.append_to_stream(stream_name, expected_version, events)
    end

    test "appends events to a stream with expected_version < actual_version" do
      stream_name = "A"
      expected_version = 3
      actual_version = expected_version + 1
      event_stream_fixture(stream_name, actual_version)
      events = events_fixture(1)

      assert {:error, :wrong_expected_version, ^actual_version} =
               StorageEngine.append_to_stream(stream_name, expected_version, events)
    end

    test "sends notification when events appended" do
      EventPublisher.subscribe()

      stream_name = "MyStream"
      expected_version = 0
      events = events_fixture(2)

      assert :ok = StorageEngine.append_to_stream(stream_name, expected_version, events)

      assert_receive {:events_appended}
    end
  end

  describe "load_event_stream/2" do
    test "load full event stream" do
      stream_name = "MyStream"

      events = [
        event_fixture(%{event_type: "A", event_body: <<1>>}),
        event_fixture(%{event_type: "B", event_body: <<2>>}),
        event_fixture(%{event_type: "C", event_body: <<3>>})
      ]

      event_stream_fixture(stream_name, events)

      # Another event stream
      event_stream_fixture()

      assert {3,
              [
                %StoredEvent{
                  event_type: "A",
                  event_body: <<1>>,
                  stream_name: ^stream_name,
                  stream_version: 1
                },
                %StoredEvent{
                  event_type: "B",
                  event_body: <<2>>,
                  stream_name: ^stream_name,
                  stream_version: 2
                },
                %StoredEvent{
                  event_type: "C",
                  event_body: <<3>>,
                  stream_name: ^stream_name,
                  stream_version: 3
                }
              ]} = StorageEngine.load_event_stream(stream_name, 0)
    end

    test "load partial event stream" do
      stream_name = "MyStream"

      events = [
        event_fixture(),
        event_fixture(),
        event_fixture(%{event_type: "C"}),
        event_fixture(%{event_type: "D"})
      ]

      event_stream_fixture(stream_name, events)

      # Another event stream
      event_stream_fixture()

      assert {4,
              [
                %StoredEvent{event_type: "C"},
                %StoredEvent{event_type: "D"}
              ]} = StorageEngine.load_event_stream(stream_name, 2)
    end
  end

  describe "event_stream_exists/1" do
    test "returns true when an event stream exists" do
      stream_name = "MyStream"
      event_stream_fixture(stream_name)

      assert true == StorageEngine.event_stream_exists?(stream_name)
    end

    test "returns false when an event stream does not exist" do
      event_stream_fixture()

      assert false == StorageEngine.event_stream_exists?("dummy_name")
    end
  end

  describe "delete_event_stream/1" do
    test "returns true when deleting an existing stream" do
      stream_name1 = "MyStream1"
      event_stream_fixture(stream_name1)

      stream_name2 = "MyStream2"
      event_stream_fixture(stream_name2)

      assert true == StorageEngine.delete_event_stream(stream_name1)
      assert false == StorageEngine.event_stream_exists?(stream_name1)
      assert true == StorageEngine.event_stream_exists?(stream_name2)
    end

    test "returns false when deleted an non-existing stream" do
      assert false == StorageEngine.delete_event_stream("MyStream")
    end
  end

  describe "all_stored_events_after/1" do
    setup do
      stream1_name = "My1stStream"

      stream1_events = [
        event_fixture(%{event_type: "A"}),
        event_fixture(%{event_type: "B"})
      ]

      stream2_name = "My2ndStream"

      stream2_events = [
        event_fixture(%{event_type: "C"}),
        event_fixture(%{event_type: "D"})
      ]

      :ok = StorageEngine.append_to_stream(stream1_name, 0, [hd(stream1_events)])
      :ok = StorageEngine.append_to_stream(stream2_name, 0, stream2_events)
      :ok = StorageEngine.append_to_stream(stream1_name, 1, tl(stream1_events))
    end

    test "returns all the stored events from the beginning" do
      assert [
               %StoredEvent{event_type: "A"} = evt1,
               %StoredEvent{event_type: "C"} = evt2,
               %StoredEvent{event_type: "D"} = evt3,
               %StoredEvent{event_type: "B"} = evt4
             ] = StorageEngine.all_stored_events_after(0)

      assert evt1.event_id < evt2.event_id
      assert evt2.event_id < evt3.event_id
      assert evt3.event_id < evt4.event_id
    end

    test "returns all the stored events after a given event" do
      [_, %{event_id: event_id}, _, _] = StorageEngine.all_stored_events_after(0)

      assert [
               %StoredEvent{event_type: "D"} = evt1,
               %StoredEvent{event_type: "B"} = evt2
             ] = StorageEngine.all_stored_events_after(event_id)

      assert evt1.event_id < evt2.event_id
    end
  end
end
