defmodule ExentStore.SubscriptionManager.SubscribeToAllTest do
  use ExentStore.DataCase

  alias ExentStore.SubscriptionManager.SubscribeToAll

  describe "new/1" do
    test "with valid data returns a new command" do
      attrs = %{
        after_event_id: 0,
        callback_url: "http://iam"
      }

      assert {:ok,
              %SubscribeToAll{
                after_event_id: 0,
                callback_url: "http://iam"
              }} = SubscribeToAll.new(attrs)
    end

    test "with missing data returns an error changeset" do
      assert {:error, %Ecto.Changeset{action: :subscribe} = changeset} = SubscribeToAll.new(%{})
      assert %{after_event_id: ["can't be blank"]} = errors_on(changeset)
      assert %{callback_url: ["can't be blank"]} = errors_on(changeset)
    end

    test "with negative after event id returns an error changeset" do
      attrs = %{
        after_event_id: -1,
        callback_url: "http://iam"
      }

      assert {:error, %Ecto.Changeset{} = changeset} = SubscribeToAll.new(attrs)
      assert %{after_event_id: ["must be greater than or equal to 0"]} = errors_on(changeset)
    end

    test "with non-unique callback URL returns an error changeset" do
      attrs = %{
        after_event_id: 0,
        callback_url: "http://iam"
      }

      subscription_fixture(attrs.after_event_id, attrs.callback_url)

      assert {:error, %Ecto.Changeset{} = changeset} = SubscribeToAll.new(attrs)
      assert %{callback_url: ["must be unique"]} = errors_on(changeset)
    end
  end
end
