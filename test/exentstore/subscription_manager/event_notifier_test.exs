defmodule ExentStore.SubscriptionManager.EventNotifierTest do
  use ExentStore.DataCase

  import Mox

  alias ExentStore.Http.MockClient
  alias ExentStore.StorageEngine
  alias ExentStore.SubscriptionManager.EventNotifier

  setup :set_mox_from_context
  setup :verify_on_exit!

  setup do
    subscription = subscription_fixture(0, "http://iam")

    notifier = start_supervised!({EventNotifier, {subscription}})

    %{
      subscription: subscription,
      notifier: notifier
    }
  end

  test "are temporary workers" do
    assert Supervisor.child_spec(EventNotifier, []).restart == :temporary
  end

  test "is alive when started", %{notifier: notifier, subscription: subscription} do
    assert %{subscription: ^subscription} = :sys.get_state(notifier)
  end

  test "delivers events when new events appended", %{subscription: %{callback_url: callback_url}} do
    events = events_fixture(2)

    MockClient
    |> expect(:post, fn ^callback_url, _body ->
      :ok
    end)

    StorageEngine.append_to_stream("MyStream", 0, events)

    Process.sleep(500)
  end
end
