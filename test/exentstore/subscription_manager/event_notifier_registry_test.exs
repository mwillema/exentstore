defmodule ExentStore.SubscriptionManager.EventNotifierRegistryTest do
  use ExentStore.DataCase

  alias ExentStore.SubscriptionManager

  alias ExentStore.SubscriptionManager.{
    EventNotifierRegistry,
    EventNotifierSupervisor
  }

  describe "without existing subscriptions" do
    setup do
      start_supervised!(
        {DynamicSupervisor, strategy: :one_for_one, name: EventNotifierSupervisor}
      )

      registry = start_supervised!(EventNotifierRegistry)

      %{registry: registry}
    end

    test "is empty when started", %{registry: registry} do
      assert {subscription_ids, refs} = :sys.get_state(registry)
      assert subscription_ids == %{}
      assert refs == %{}
    end

    test "spawns notifiers", %{registry: registry} do
      %{id: subscription_id} = SubscriptionManager.subscribe_to_all!(0, "http://iam")

      Process.sleep(500)

      assert {%{^subscription_id => notifier}, refs} = :sys.get_state(registry)
      assert Process.alive?(notifier)
      assert Enum.count(refs) == 1
      assert {_ref, ^subscription_id} = Enum.at(refs, 0)
    end

    test "removes notifiers on exit", %{registry: registry} do
      %{id: subscription_id} = SubscriptionManager.subscribe_to_all!(0, "http://iam")

      Process.sleep(500)

      {%{^subscription_id => notifier}, _refs} = :sys.get_state(registry)

      # Stop notifier with normal reason
      GenServer.stop(notifier)

      {subscription_ids, refs} = :sys.get_state(registry)
      assert subscription_ids == %{}
      assert refs == %{}
    end

    test "removes notifiers on crash", %{registry: registry} do
      %{id: subscription_id} = SubscriptionManager.subscribe_to_all!(0, "http://iam")

      Process.sleep(500)

      {%{^subscription_id => notifier}, _refs} = :sys.get_state(registry)

      # Stop notifier with non-normal reason
      GenServer.stop(notifier, :shutdown)

      {subscription_ids, refs} = :sys.get_state(registry)
      assert subscription_ids == %{}
      assert refs == %{}
    end
  end

  describe "with existing subscriptions" do
    setup do
      %{id: subscription_id1} = SubscriptionManager.subscribe_to_all!(0, "http://iam1")
      %{id: subscription_id2} = SubscriptionManager.subscribe_to_all!(10, "http://iam2")

      start_supervised!(
        {DynamicSupervisor, strategy: :one_for_one, name: EventNotifierSupervisor}
      )

      registry = start_supervised!(EventNotifierRegistry)

      %{
        registry: registry,
        subscription_id1: subscription_id1,
        subscription_id2: subscription_id2
      }
    end

    test "is not empty when started", %{
      registry: registry,
      subscription_id1: subscription_id1,
      subscription_id2: subscription_id2
    } do
      assert {%{
                ^subscription_id1 => notifier1,
                ^subscription_id2 => notifier2
              }, refs} = :sys.get_state(registry)

      assert Process.alive?(notifier1)
      assert Process.alive?(notifier2)

      assert Enum.count(refs) == 2
      assert {_ref, ^subscription_id1} = Enum.at(refs, 0)
      assert {_ref, ^subscription_id2} = Enum.at(refs, 1)
    end
  end
end
