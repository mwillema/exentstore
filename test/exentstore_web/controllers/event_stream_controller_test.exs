defmodule ExentStoreWeb.EventStreamControllerTest do
  use ExentStoreWeb.ConnCase

  alias ExentStore.StorageEngine

  setup %{conn: conn} do
    conn =
      conn
      |> put_req_header("accept", "application/json")

    %{conn: conn}
  end

  describe "append to stream" do
    test "renders created when data is valid", %{conn: conn} do
      stream_name = "MyStream"

      attrs = %{
        expected_version: 2,
        events: [
          %{event_type: "A", event_body: <<0>>},
          %{event_type: "B", event_body: <<1>>}
        ]
      }

      event_stream_fixture(stream_name, attrs.expected_version)

      conn = post(conn, Routes.event_stream_path(conn, :create, stream_name), attrs)
      assert response(conn, 201)

      assert 4 = StorageEngine.count_stored_events()
    end

    test "renders error when data is invalid", %{conn: conn} do
      conn = post(conn, Routes.event_stream_path(conn, :create, "MyStream"), %{})
      assert json_response(conn, 422)["errors"] != %{}
    end

    test "renders error when wrong expected version", %{conn: conn} do
      stream_name = "MyStream"

      attrs = %{
        expected_version: 1,
        events: [
          %{event_type: "A", event_body: <<0>>}
        ]
      }

      event_stream_fixture(stream_name, attrs.expected_version + 1)

      conn = post(conn, Routes.event_stream_path(conn, :create, stream_name), attrs)

      assert %{
               "error" => "wrong expected version",
               "actual_version" => 2
             } = json_response(conn, 422)
    end
  end

  describe "load event stream" do
    test "renders events and version when data is valid", %{conn: conn} do
      stream_name = "MyStream"

      attrs = %{after_stream_version: 1}

      events = [
        event_fixture(),
        event_fixture(%{event_type: "B", event_body: <<2>>}),
        event_fixture(%{event_type: "C", event_body: <<3>>})
      ]

      event_stream_fixture(stream_name, events)

      {stream_version,
       [
         %{event_id: event_id1, occurred_on: occurred_on1},
         %{event_id: event_id2, occurred_on: occurred_on2}
       ]} = StorageEngine.load_event_stream(stream_name, attrs.after_stream_version)

      occurred_on1 = NaiveDateTime.to_iso8601(occurred_on1)
      occurred_on2 = NaiveDateTime.to_iso8601(occurred_on2)

      previous_stream_version = stream_version - 1

      conn = get(conn, Routes.event_stream_path(conn, :show, stream_name), attrs)

      assert %{
               "stream_version" => ^stream_version,
               "events" => [
                 %{
                   "event_id" => ^event_id1,
                   "event_type" => "B",
                   "event_body" => <<2>>,
                   "occurred_on" => ^occurred_on1,
                   "stream_name" => ^stream_name,
                   "stream_version" => ^previous_stream_version
                 },
                 %{
                   "event_id" => ^event_id2,
                   "event_type" => "C",
                   "event_body" => <<3>>,
                   "occurred_on" => ^occurred_on2,
                   "stream_name" => ^stream_name,
                   "stream_version" => ^stream_version
                 }
               ]
             } = json_response(conn, 200)
    end

    test "renders not found when stream does not exist", %{conn: conn} do
      stream_name = "MyStream"
      attrs = %{after_stream_version: 0}

      conn = get(conn, Routes.event_stream_path(conn, :show, stream_name), attrs)

      assert json_response(conn, 404)
    end

    test "renders error when data is invalid", %{conn: conn} do
      conn = post(conn, Routes.event_stream_path(conn, :show, "MyStream"), %{})
      assert json_response(conn, 422)["errors"] != %{}
    end
  end

  describe "event stream exists" do
    test "renders 200 when event stream exists", %{conn: conn} do
      stream_name = "MyStream"

      event_stream_fixture(stream_name)

      conn = head(conn, Routes.event_stream_path(conn, :show, stream_name))

      assert response(conn, 200)
    end

    test "renders 404 when event stream does not exist", %{conn: conn} do
      stream_name = "MyStream"

      conn = head(conn, Routes.event_stream_path(conn, :show, stream_name))

      assert response(conn, 404)
    end
  end

  describe "delete event stream" do
    test "renders 204 when deleting an existing event stream", %{conn: conn} do
      stream_name = "MyStream"

      event_stream_fixture(stream_name)

      conn = delete(conn, Routes.event_stream_path(conn, :delete, stream_name))

      assert response(conn, 204)
    end

    test "renders 404 when deleting an non-existing event stream", %{conn: conn} do
      conn = delete(conn, Routes.event_stream_path(conn, :delete, "MyStream"))

      assert response(conn, 404)
    end
  end
end
