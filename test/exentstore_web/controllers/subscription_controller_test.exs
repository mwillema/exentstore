defmodule ExentStoreWeb.SubscriptionControllerTest do
  use ExentStoreWeb.ConnCase

  setup %{conn: conn} do
    conn =
      conn
      |> put_req_header("accept", "application/json")

    %{conn: conn}
  end

  describe "subscribe to all" do
    test "renders subscription when data is valid", %{conn: conn} do
      after_event_id = 0
      callback_url = "http://iam"

      attrs = %{
        after_event_id: after_event_id,
        callback_url: callback_url
      }

      conn = post(conn, Routes.subscription_path(conn, :create), attrs)

      assert %{
               "subscription_id" => _id,
               "after_event_id" => ^after_event_id,
               "callback_url" => ^callback_url
             } = json_response(conn, 201)
    end

    test "renders error when data is invalid", %{conn: conn} do
      conn = post(conn, Routes.subscription_path(conn, :create), %{})
      assert json_response(conn, 422)["errors"] != %{}
    end
  end
end
