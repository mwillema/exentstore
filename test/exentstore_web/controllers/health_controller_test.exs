defmodule ExentStoreWeb.HealthControllerTest do
  use ExentStoreWeb.ConnCase

  setup %{conn: conn} do
    conn =
      conn
      |> put_req_header("accept", "application/json")

    %{conn: conn}
  end

  describe "health" do
    test "renders 200 when the service is up and running", %{conn: conn} do
      conn = get(conn, Routes.health_path(conn, :health))

      assert %{"status" => "UP"} = json_response(conn, 200)
    end
  end
end
