defmodule ExentStore.TestHelpers do
  alias ExentStore.{StorageEngine, SubscriptionManager}

  alias ExentStore.StorageEngine.Event

  def event_fixture(attrs \\ %{}) do
    {:ok, event} =
      attrs
      |> Enum.into(%{
        event_type: attrs[:event_type] || "MyEvent",
        event_body: attrs[:event_body] || <<0>>
      })
      |> Event.new()

    event
  end

  def events_fixture(event_count) do
    event_fixture()
    |> List.duplicate(event_count)
  end

  def event_stream_fixture do
    stream_name = Ecto.UUID.generate()
    event_stream_fixture(stream_name)
  end

  def event_stream_fixture(stream_name) do
    stream_version = Enum.random(1..10)
    event_stream_fixture(stream_name, stream_version)
  end

  def event_stream_fixture(stream_name, stream_version)
      when is_integer(stream_version) and stream_version > 0 do
    events = events_fixture(stream_version)
    event_stream_fixture(stream_name, events)
  end

  def event_stream_fixture(stream_name, events) when is_list(events) do
    :ok = StorageEngine.append_to_stream(stream_name, 0, events)
  end

  def subscription_fixture(after_event_id, callback_url) do
    SubscriptionManager.subscribe_to_all!(after_event_id, callback_url)
  end
end
