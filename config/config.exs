# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.

# General application configuration
use Mix.Config

config :exentstore,
  namespace: ExentStore,
  ecto_repos: [ExentStore.Repo],
  env: Mix.env(),
  http_client: ExentStore.Http.HttpoisonClient

# Configures the endpoint
config :exentstore, ExentStoreWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "avQcINCXs+0QVWY4rN14CRAC/PK/jvVy47ZHhr0M2WsyOkL7tw0Q7TE8r4S/fw0m",
  render_errors: [view: ExentStoreWeb.ErrorView, accepts: ~w(html json), layout: false],
  pubsub_server: ExentStore.PubSub,
  live_view: [signing_salt: "yw0Sk+o1"]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Use Jason for JSON parsing in Phoenix
config :phoenix, :json_library, Jason

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env()}.exs"
